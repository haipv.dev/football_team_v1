package com.appian.manutdvietnam.app;

public interface ToolbarViewListener {
    void changeToolbarTitle(String title);
}
