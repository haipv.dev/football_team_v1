package com.appian.manutdvietnam.app.team.view;

import com.appnet.android.football.sofa.data.Performance;

import java.util.List;

public interface TeamPerformanceView {
    void showTeamPerformance(List<Performance> data);
}
