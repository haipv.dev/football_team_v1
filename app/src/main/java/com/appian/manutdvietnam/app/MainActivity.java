package com.appian.manutdvietnam.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appian.manutdvietnam.R;
import com.appian.manutdvietnam.app.adapter.NavigationListAdapter;
import com.appian.manutdvietnam.app.home.presenter.SeasonLeagueTeamPresenter;
import com.appian.manutdvietnam.app.home.view.SeasonLeagueTeamView;
import com.appian.manutdvietnam.app.league.LeagueFragment;
import com.appian.manutdvietnam.app.newstopic.NewsTopicFragment;
import com.appian.manutdvietnam.app.setting.SettingActivity;
import com.appian.manutdvietnam.app.team.TeamFragment;
import com.appian.manutdvietnam.app.user.LogInActivity;
import com.appian.manutdvietnam.app.user.OnBtnLogoutClickListener;
import com.appian.manutdvietnam.app.user.UserFragment;
import com.appian.manutdvietnam.data.account.AccountManager;
import com.appian.manutdvietnam.data.account.UserAccount;
import com.appian.manutdvietnam.data.app.AppConfig;
import com.appian.manutdvietnam.data.app.AppConfigManager;
import com.appian.manutdvietnam.data.app.Language;
import com.appian.manutdvietnam.network.ConnectivityEvent;
import com.appian.manutdvietnam.receiver.ReceiverHelper;
import com.appian.manutdvietnam.util.ImageLoader;
import com.appnet.android.ads.admob.InterstitialAdMob;
import com.appnet.android.football.fbvn.data.LeagueSeason;
import com.appnet.android.social.auth.OnLogoutListener;
import com.github.fernandodev.easyratingdialog.library.EasyRatingDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, ToolbarViewListener,
        OnLogoutListener, OnBtnLogoutClickListener,
        SeasonLeagueTeamView {
    private static final String TAG_FRAGMENT_LEAGUE = "fragment_league";
    private static final String TAG_FRAGMENT_HOME = "fragment_home";
    private static final String TAG_FRAGMENT_TOPIC = "fragment_topic";
    private static final String TAG_FRAGMENT_SQUAD = "fragment_squad";
    private static final String TAG_FRAGMENT_PROFILE = "fragment_profile";

    private boolean doubleBackToExitPressedOnce = false;

    private static final int RC_SETTING = 1;
    private static final int RC_LOGIN = 2;

    private TextView txtTitle;

    private ImageView toggleBtn;
    private DrawerLayout mDrawerLayout;
    private NavigationListAdapter mNavigationAdapter;
    private List<LeagueSeason> mLeagueSesons;
    private TextView mTvProfileName;

    private ImageView mImgUserProfile;

    private InterstitialAdMob mInterstitialAdMob;

    private EasyRatingDialog easyRatingDialog;

    private BroadcastReceiver mUserProfileChangedReceiver;

    private SeasonLeagueTeamPresenter mSeasonLeagueTeamPresenter;

    private boolean mIShowAds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLeagueSesons = new ArrayList<>();
        txtTitle = findViewById(R.id.txtTitle);
        mDrawerLayout = findViewById(R.id.drawer_layout);

        easyRatingDialog = new EasyRatingDialog(this);

        //User profile
        mImgUserProfile = findViewById(R.id.profileImage);
        mTvProfileName = findViewById(R.id.tv_profile_name);

        toggleBtn = findViewById(R.id.main_menu_button_toggle);
        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleBtn.isSelected()) {
                    toggleBtn.setSelected(false);
                } else {
                    toggleBtn.setSelected(true);
                }
                if (mDrawerLayout.isDrawerOpen(Gravity.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    mDrawerLayout.openDrawer(Gravity.START);
                }
            }
        });

        switchFragment(TAG_FRAGMENT_HOME, null);
        mNavigationAdapter = new NavigationListAdapter(this, mLeagueSesons);
        ListView navigationList = findViewById(R.id.navigation_list);
        navigationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LeagueSeason leagueSeason = mLeagueSesons.get(position);
                Bundle args = new Bundle();
                args.putInt("league_id", leagueSeason.getSofaLeagueId());
                args.putInt("season_id", leagueSeason.getSofaId());
                args.putString("league_name", leagueSeason.getLeagueName());
                switchFragment(TAG_FRAGMENT_LEAGUE, args);
            }
        });
        navigationList.setAdapter(mNavigationAdapter);
        View viewNewsFeedMenu = findViewById(R.id.llNewsFeedMenu);
        View viewSquadMenu = findViewById(R.id.llSquadMenu);
        View viewTopicMenu = findViewById(R.id.llTopicMenu);
        View viewSetting = findViewById(R.id.rl_setting);
        View viewProfile = findViewById(R.id.profile_layout);
        viewNewsFeedMenu.setOnClickListener(this);
        viewSquadMenu.setOnClickListener(this);
        viewTopicMenu.setOnClickListener(this);
        viewSetting.setOnClickListener(this);
        viewProfile.setOnClickListener(this);
        mSeasonLeagueTeamPresenter = new SeasonLeagueTeamPresenter();
        mSeasonLeagueTeamPresenter.attachView(this);
        loadLeftMenu();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        handleLogin();
        AppConfig appConfig = AppConfigManager.getInstance().getAppConfig(this);
        mInterstitialAdMob = new InterstitialAdMob(this, appConfig.getAdmobInterstitial());

        mUserProfileChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleLogin();
            }
        };
        View btnShare = findViewById(R.id.btn_share);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareVia();
            }
        });
        ReceiverHelper.registerUserProfileChanged(this, mUserProfileChangedReceiver);
    }

    private void shareVia() {
        AppConfig appConfig = AppConfigManager.getInstance().getAppConfig(this);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, appConfig.getShareContent());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_via)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerEventBus(true);
        easyRatingDialog.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        easyRatingDialog.showIfNeeded();
        mInterstitialAdMob.loadAd();
        mIShowAds = false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onBackPressed() {
        //show ad after pressing back
        FragmentManager fm = getSupportFragmentManager();
        Fragment homeFragment = fm.findFragmentByTag(TAG_FRAGMENT_HOME);
        if (homeFragment != null && homeFragment.isVisible()) {
            if(!mIShowAds) {
                mInterstitialAdMob.show();
                mIShowAds = true;
            }
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.confirm_exit, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            switchFragment(TAG_FRAGMENT_HOME);
        }
    }

    private void loadLeftMenu() {
        AppConfig appConfig = AppConfigManager.getInstance().getAppConfig(this);
        mSeasonLeagueTeamPresenter.loadSeasonLeaguesByTeam(appConfig.getCurrentSeasonId(), appConfig.getTeamId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ConnectivityEvent event) {
        if (event.isConnected()) {
            if (mLeagueSesons != null && mLeagueSesons.isEmpty()) {
                loadLeftMenu();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llNewsFeedMenu:
                switchFragment(TAG_FRAGMENT_HOME);
                break;
            case R.id.llSquadMenu:
                switchFragment(TAG_FRAGMENT_SQUAD);
                break;
            case R.id.llTopicMenu:
                switchFragment(TAG_FRAGMENT_TOPIC);
                break;
            case R.id.rl_setting:
                switchActivity(RC_SETTING);
                break;
            case R.id.profile_layout:
                UserAccount userAccount = AccountManager.getInstance().getAccount(this);
                if (userAccount != null) {
                    switchFragment(TAG_FRAGMENT_PROFILE);
                } else {
                    switchActivity(RC_LOGIN);
                }
                break;
            default:
                switchFragment(TAG_FRAGMENT_HOME);
                break;

        }
    }

    private void switchFragment(String tag) {
        switchFragment(tag, null);
    }

    private void switchFragment(String tag, Bundle args) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentByTag(tag);
        if (TAG_FRAGMENT_HOME.equals(tag)) {
            if (fragment == null) {
                fragment = new HomeFragment();
            }
        } else if (TAG_FRAGMENT_TOPIC.equals(tag)) {
            if (fragment == null) {
                fragment = new NewsTopicFragment();
            }
        } else if (TAG_FRAGMENT_LEAGUE.equals(tag)) {
            if (fragment == null) {
                fragment = LeagueFragment.newInstance(args);
            } else if (fragment instanceof LeagueFragment) {
                ((LeagueFragment) fragment).updateLeagueSeason(args);
            }
        } else if (TAG_FRAGMENT_SQUAD.equals(tag)) {
            if (fragment == null) {
                fragment = new TeamFragment();
            }
        } else if (TAG_FRAGMENT_PROFILE.equals(tag)) {
            if (fragment == null) {
                fragment = new UserFragment();
            }
        }
        if (fragment != null) {
            fm.beginTransaction().replace(R.id.main_layout_container, fragment, tag).commit();
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void switchActivity(int requestCode) {
        Intent intent;
        switch (requestCode) {
            case RC_SETTING:
                intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
                break;
            case RC_LOGIN:
                intent = new Intent(this, LogInActivity.class);
                startActivityForResult(intent, RC_LOGIN);
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void changeToolbarTitle(String title) {
        txtTitle.setText(title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ReceiverHelper.unregisterReceiver(this, mUserProfileChangedReceiver);
        mSeasonLeagueTeamPresenter.detachView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_LOGIN && resultCode == RESULT_OK) {
            handleLogin();
        }
    }

    private void handleLogin() {
        UserAccount account = AccountManager.getInstance().getAccount(this);
        if (account == null) {
            fillNotLoginUser();
            return;
        }
        ImageLoader.displayImage(account.getAvatar(), mImgUserProfile, R.drawable.profile_pic);
        mTvProfileName.setText(account.getFullName());
    }

    private void handleLogout() {
        AccountManager.getInstance().clearAccount(this);
        fillNotLoginUser();
        switchFragment(TAG_FRAGMENT_HOME);
    }

    private void fillNotLoginUser() {
        mImgUserProfile.setImageResource(R.drawable.profile_pic);
        mTvProfileName.setText(getResources().getString(R.string.log_in_menu));
    }

    @Override
    public void onLogoutSuccess() {
        fillNotLoginUser();
    }

    @Override
    public void onBtnLogoutClick() {
        handleLogout();
    }

    @Override
    public void showSeasonLeagueTeam(List<LeagueSeason> data) {
        mLeagueSesons.clear();
        mLeagueSesons.addAll(data);
        mNavigationAdapter.notifyDataSetChanged();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(Language.onAttach(newBase));
    }

    @Override
    protected void onStop() {
        super.onStop();
        registerEventBus(false);
    }

    private void registerEventBus(boolean isRegister) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }
        if (isRegister) {
            EventBus.getDefault().register(this);
        } else {
            EventBus.getDefault().unregister(this);
        }
    }
}