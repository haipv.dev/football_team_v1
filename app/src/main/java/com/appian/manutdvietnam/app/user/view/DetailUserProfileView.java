package com.appian.manutdvietnam.app.user.view;

import com.appnet.android.football.fbvn.data.AccountProfile;

public interface DetailUserProfileView {
    void showUserProfile(AccountProfile data);
}
