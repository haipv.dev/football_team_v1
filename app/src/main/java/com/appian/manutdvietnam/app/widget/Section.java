package com.appian.manutdvietnam.app.widget;

import java.util.List;

public interface Section<C> {
    List<C> getChildItem();
}
