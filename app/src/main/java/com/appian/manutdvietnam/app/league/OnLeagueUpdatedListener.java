package com.appian.manutdvietnam.app.league;


public interface OnLeagueUpdatedListener {
    void onLeagueUpdated(int leagueId, int seasonId);
}
