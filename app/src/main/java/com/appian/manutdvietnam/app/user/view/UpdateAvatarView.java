package com.appian.manutdvietnam.app.user.view;


public interface UpdateAvatarView {
    void updateAvatarSuccess(String path);
    void updateAvatarFail();
}
