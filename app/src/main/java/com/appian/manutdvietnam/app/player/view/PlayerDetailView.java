package com.appian.manutdvietnam.app.player.view;

import com.appnet.android.football.sofa.data.Player;

public interface PlayerDetailView {
    void showPlayerDetail(Player data);
}
