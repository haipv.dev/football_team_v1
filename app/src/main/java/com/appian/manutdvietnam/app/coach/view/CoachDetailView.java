package com.appian.manutdvietnam.app.coach.view;

import com.appnet.android.football.sofa.data.Manager;

public interface CoachDetailView {
    void showCoachDetail(Manager data);
}
