package com.appian.manutdvietnam.app.match;

import com.appnet.android.football.sofa.data.Event;

public interface OnMatchUpdatedListener {
    void onMatchUpdated(Event event);
}
