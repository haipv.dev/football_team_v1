package com.appian.manutdvietnam.app.match.view;

import com.appnet.android.football.sofa.data.LineupsData;

public interface MatchLineupsView {
    void showMatchLineups(LineupsData data);
}
