package com.appian.manutdvietnam.app.match.view;

import com.appnet.android.football.sofa.data.StatisticsData;

public interface MatchStatisticView {
    void showMatchStatistic(StatisticsData data);
}
