package com.appian.manutdvietnam.app.detailnews.view;

import com.appnet.android.football.fbvn.data.News;

public interface DetailNewsView {
    void showNews(News news);
}
